import React from 'react'
import ReactDOM from 'react-dom'
import { ConfigProvider } from 'antd';
import zhCN from '../node_modules/antd/lib/locale/zh_CN'

import AppRouter from './AppRouter'
import './assets/less/common.less'

ReactDOM.render(
<ConfigProvider locale={zhCN}>
    <AppRouter />
</ConfigProvider>
,document.getElementById('root'))