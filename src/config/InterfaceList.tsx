// location接口
export interface LocationAuth{
    hash:string
    pathname:string
    search?:string
    [propName:string]:any
}
// 路由接口
export interface RouterParamsAuth {
    name:string
    icon:string
    exact:boolean
    path:string
    [propName:string]:any
}
export interface Props{
    computedMatch?:Readonly<{}>,
    config:RouterParamsAuth[],
    location?:LocationAuth
}