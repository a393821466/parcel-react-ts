import React from 'react';
import { Route,Redirect } from 'react-router-dom';
import {Props,RouterParamsAuth} from '../config/InterfaceList'

// 路由验权
const RouterAuth=(props:Props)=>{
    const {location,config}=props
    const token=localStorage.getItem('token')
    // 容错
    if(!location){
        return <Redirect to='/' />
    }
    const routerList=config.find((v:RouterParamsAuth)=>v.path===location.pathname)
    if(routerList && !routerList.auth && !token){
        const {component,path}=routerList
        return <Route exact path={path} component={component}/>
    }
    // 登录状态
    if(token){
        if(location.pathname==='/auth/login'){
            return <Redirect to='/' />
        }else{
            // 路由存在情况下
            if(routerList){
                return <Route path={location.pathname} component={routerList.component}/>
            }else{
                return <Redirect to='/404' />
            }
        }
    }else{
        if(routerList && routerList.auth){
            return <Redirect to='/auth/login' />
        }else{
            return <Redirect to='/404' />
        }
    }

}
export default RouterAuth