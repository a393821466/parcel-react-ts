import React from 'react'
export default [
    {
        name: '首页',
        icon: 'home',
        exact: true,
        path:'/',
        component:React.lazy(()=>import('./pages/Home/index'))
    },
    {
        name: '详情',
        icon: 'details',
        exact: false,
        path:'/details',
        component:React.lazy(()=>import('./pages/Details/index'))
    }
]