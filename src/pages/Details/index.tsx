import React,{useState} from 'react'
import {Input} from 'antd'
import styles from './style.less'
const Details=()=>{
    const [value,setValue]=useState('-')
    const onHandlerChange=(e: { target: { value: string } })=>{
        setValue(e.target.value)
    }
    return (
        <div>
            <div className={styles['box']}>
                <h1>详情页</h1>
                <Input type="text" className={styles['inp']}  onChange={onHandlerChange} />
                <h2 style={{marginTop:20}}>输出：<span style={{color:'red'}}>{value}</span></h2>
            </div>
        </div>
    )
}

export default Details