import React from 'react'
import {Button,message} from 'antd'
import styles from './style.less'

const Home=()=>{
    return (
        <div className={styles['container']}>
            <h1>首页</h1>
            <Button type="primary" onClick={()=>{message.error('什么都没有阿...哈哈哈')}}>按钮</Button>
        </div>
    )
}

export default Home