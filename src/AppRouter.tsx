import React,{useState,SetStateAction} from 'react'
import {Link,Switch,HashRouter} from 'react-router-dom'
import RouterAuth from './config/RouterAuth'
import routes from './router'

import { Menu } from 'antd'

// 路由配置
const AppRouter=()=>{
    const [current,setCurrent] = useState('/')

    const handleClick=(e:{key: SetStateAction<string>})=>{
        setCurrent(e.key)
    }
    return (
        <div>
            <HashRouter>
                <div className="nav" style={{marginBottom:50}}>
                    <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
                        <Menu.Item key="/">
                            <Link to="/">首页</Link>
                        </Menu.Item>
                        <Menu.Item key="details">
                            <Link to="/details">详情</Link>
                        </Menu.Item>
                    </Menu>
                </div>
                <React.Suspense fallback={<div>loading...</div>}>
                    <Switch>
                        <RouterAuth config={routes} />
                    </Switch>
                </React.Suspense>
            </HashRouter>
        </div>
    )
}

export default AppRouter